<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/7/16
 * Time: 9:28 AM
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Client</div>
                    <div class="panel-body">

                        <div class="card">
                            <div class="card-heading b-b">

{!! Form::open(array('url' => 'clients/updateClient/'.$client['clientId'],'method'=>'POST','autocomplete'=>'off')) !!}
<table align="center">
    <tr>
        <td>
            {!!  Form::text('name',$client['name'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('name', 'First Name*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('lname',$client['personName'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('lname', 'Person Name*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('emails',$client['emails'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('emails', 'Emails*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('phones',$client['phones'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('phones', 'Phones*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!! Form::submit('update',array('class' => 'md-btn md-raised m-b btn-fw green waves-effect')) !!}
            <a href="{{URL::to('clients/')}}"><input type="button" style="margin-left: 20px"
                                                     class="md-btn md-raised m-b btn-fw white waves-effect"
                                                     name="cancel" value="cancel"></a>
        </td>
    </tr>
    {!! Form::close() !!}
</table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection




