
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Client</div>
                    <div class="panel-body">

                        <div class="card">
                            <div class="card-heading b-b">

                            </div>
    {!! Form::open(array('url' => 'clients/addClient','method'=>'POST','autocomplete'=>'off')) !!}
    <div class="card-body">
        <div class="row row-sm">
            <div class="col-sm-5">
                <div class="md-form-group float-label">
                    {!!  Form::text('name', Input::old('name'), array('class' => 'md-input','required')) !!}
                    {!!  Form::label('name', 'Client Name*') !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="md-form-group float-label">
                    {!!  Form::text('personName', Input::old('personName'), array('class' => 'md-input','required')) !!}
                    {!!  Form::label('personName', 'Person Name*') !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="md-form-group float-label">
                    {!!  Form::text('emails', Input::old('emails'), array('class' => 'md-input','required')) !!}
                    {!!  Form::label('emails', 'Emails*') !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="md-form-group float-label">
                    {!!  Form::text('phones', Input::old('phones'), array('class' => 'md-input','required')) !!}
                    {!!  Form::label('phones', 'Phones*') !!}
                </div>
            </div>
        </div>
        <div class="md-form-group float-label">
            {!! Form::submit('Add',array('class' => 'md-btn md-raised m-b btn-fw green waves-effect')) !!}
            <a href="{{URL::to('clients/')}}"><input type="button" style="margin-left: 20px"
                                                     class="md-btn md-raised m-b btn-fw white waves-effect"
                                                     name="cancel" value="cancel"></a>
        </div>
        {!! Form::close() !!}
    </div>
</div>  </div>
                </div>
            </div>
        </div>
    </div>
@endsection