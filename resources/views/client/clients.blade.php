@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Clients</div>

                    <div class="panel-body">
<table border="2" align="center"  cellspacing="10px">
<?php
$i = 1;
foreach($clients as $client)
{
?>

<tr >
    <td>#{{$i}}</td>

    <td>{{$client->name}}</td>
    <td>
        {{$client->personName}}
    </td>
    <td>
        {{$client->phones}}
    </td>
    <td>
        {{$client->emails}}
    </td>
    <td>
        <a href="{{ URL::to('clients/'.$client->clientId.'/edit')  }}"
           title="Edit">edit</a>
    </td>
    <td>
        <a href="{{ URL::to('clients/deleteClient/'.$client->clientId)  }}"
           title="Delete">delete</a>
    </td>


    <?php
    }?>
    </table>
                        <div align="center">
                            <a href="{{URL::to('clients/create/')}}"><input type="button" style="margin-left: 20px"
                                                                     class="md-btn md-raised m-b btn-fw white waves-effect"
                                                                     name="Add New Client" value="Add New Client"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection