<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/7/16
 * Time: 9:28 AM
 */
?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Person</div>
                    <div class="panel-body">

{!! Form::open(array('url' => 'persons/updatePerson/'.$person['personId'],'method'=>'POST','autocomplete'=>'off')) !!}
<table align="center">
    <tr>
        <td>
            {!!  Form::text('fname',$person['fname'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('fname', 'First Name*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('lname',$person['lname'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('lname', 'Last Name*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('emails',$person['emails'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('emails', 'Emails*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!!  Form::text('phones',$person['phones'], array('class' => 'md-input','required')) !!}
            {!!  Form::label('phones', 'Phones*') !!}
        </td>
    </tr>
    <tr>
        <td>
            {!! Form::submit('update',array('class' => 'md-btn md-raised m-b btn-fw green waves-effect')) !!}
            <a href="{{URL::to('persons/')}}"><input type="button" style="margin-left: 20px"
                                                     class="md-btn md-raised m-b btn-fw white waves-effect"
                                                     name="cancel" value="cancel"></a>
        </td>
    </tr>
    {!! Form::close() !!}
</table>



                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection


