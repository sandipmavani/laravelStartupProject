
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Persons</div>

                    <div class="panel-body">
                        <table border="2" cellpadding=10 cellspacing=10 align="center">
                        <?php
                        $i = 1;
                        foreach($persons as $person)
                        {
                        ?>

                        <tr>
                            <td>#{{$i}}</td>
                            <td>{{$person->fname}}</td>
                            <td>
                                {{$person->fname}}
                            </td>
                            <td>
                                {{$person->phones}}
                            </td>
                            <td>
                                {{$person->emails}}
                            </td>
                            <td>
                                <a href="{{ URL::to('persons/'.$person->personId.'/edit')  }}"
                                   title="Edit">edit</a>
                            </td>
                            <td>
                                <a href="{{ URL::to('persons/deletePerson/'.$person->personId)  }}"
                                   title="Delete">delete</a>
                            </td>


    <?php
                                $i++;
    }?>
</table>
                        <div align="center">
                            <a href="{{URL::to('persons/create/')}}"><input type="button" style="margin-left: 20px"
                                                                            class="md-btn md-raised m-b btn-fw white waves-effect"
                                                                            name="Add New Person" value="Add New Person"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection