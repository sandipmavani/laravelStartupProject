<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/7/16
 * Time: 6:20 PM
 */


namespace App\Libraries\Repositories;


use App\Person;

class PersonRepository extends CoreRepository
{
	public function __construct()
	{
	}

	public function persons()
	{
		return Person::where('isActive', '=', true)->get();
	}

	public function getPersonList()
	{
		$persons = Person::all();
		$personList = [];
		$flag = true;
		foreach($persons as $person)
		{
			$personList[$person['personId']] = $person['fname'];
			if($flag == true)
			{
				$personId = $person['personId'];
				$flag = false;
			}
		}

		return $personList;
	}

	public function getPersonNameById($personId)
	{
		$person = Person::where('personId', $personId)->first();

		return $person['fname'];
	}
	public function getPersonIdByName($name)
	{
		$person = Person::where('fname', $name)->first();

		return $person['personId'];
	}
	public function addNewFieldInPerson($field)
	{
		/*$person = Person::where($field,'$exsist',true)->get();
		$person->update(array($field=>''));*/
		$persons = Person::all();
		foreach($persons as $person)
		{
			if(!isset($person[$field]))
			{

				$oldPerson = Person::where('personId', $person['personId'])->first();
				$oldPerson->$field = '';
				$oldPerson->save();
			}


		}
		

	}
}