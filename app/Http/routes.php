<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::resource('persons','PersonController');
Route::group(['middleware' => 'auth'],function ()
{
    Route::group(['prefix' => 'persons'], function ()
    {

        Route::get('/', 'PersonController@index');
        Route::get('/create', 'PersonController@create');
        Route::get('/{id}/edit', 'PersonController@edit');
        Route::post('/updatePerson/{id}', 'PersonController@update');
        Route::get('/deletePerson/{id}', 'PersonController@delete');
        Route::post('/addPerson', 'PersonController@store');

    });

    Route::group(['prefix' => 'clients'], function ()
    {

        Route::get('/', 'ClientController@index');
        Route::get('/create', 'ClientController@create');
        Route::get('/{id}/edit', 'ClientController@edit');
        Route::post('/updateClient/{id}', 'ClientController@update');
        Route::get('/deleteClient/{id}', 'ClientController@delete');
        Route::post('/addClient', 'ClientController@store');

    });
});


