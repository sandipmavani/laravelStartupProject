<?php

namespace App\Http\Controllers;

use App\Client;
use App\Libraries\Repositories\PersonRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    //
	public $personRepository;
	public function __construct(PersonRepository $personRepository)
	{
		$this->personRepository = $personRepository;
	}

	public function index()
	{

		$clients = Client::where('isActive',true)->get();
		foreach($clients as $client)
		{
			if(!empty($client['personId']))
				$client['personName'] = $this->personRepository->getPersonNameById($client['personId']);
			else
				$client['personName'] = ''; 
		}
		return view('client.clients')->with('clients', $clients);
	}

	public function create()
	{
		return view('client.addClient');
	}

	public function edit($id)
	{
		$client = Client::where('clientId', $id)->first();
		if(!empty($client['personId']))
			$client['personName'] = $this->personRepository->getPersonNameById($client['personId']);
		else
			$client['personName'] = '';
		return view('client.editClient', compact('client'));
	}

	public function update(Request $request, $id)
	{
		$object = $request->all();
		$oldClient = Client::where('clientId', $id)->first();
		$oldClient->fill($object);
		$oldClient->save();

		return Redirect::to('/clients/');
	}

	public function delete($id)
	{
		if($id != null)
		{
			$oldClient = Client::where('clientId', $id)->first();
			$oldClient->isActive = false;
			$oldClient->fill($oldClient->toArray());
			$oldClient->save();
		}

		return Redirect::to('/clients/');
	}

	public function store(Request $request)
	{
		$input = $request->all();
		$client = [];
		$client['clientId'] = md5(microtime() . rand() . rand());
		$client['name'] = $input['name'];
		$client['personId'] =$this->personRepository->getPersonIdByName($input['personName']);
		$client['emails'] = $input['emails'];
		$client['phones'] = $input['phones'];
		$client['isActive'] = true;

		$client = Client::create($client);
		return Redirect::to('/clients/');
	}
}
