<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PersonRequest;
use Validator;

class PersonController extends Controller
{
    //


	public function index()
	{

		$person = Person::where('isActive',true)->get();

		return view('person.persons')->with('persons', $person);
	}

	public function create()
	{
		return view('person.addPerson');
	}

	public function edit($id)
	{
		$person = Person::where('personId', $id)->first();

		return view('person.editPerson', compact('person'));
	}

	public function update(PersonRequest $request, $id)
	{
		$object = $request->all();
		$oldPerson = Person::where('personId', $id)->first();
		$oldPerson->fill($object);
		$oldPerson->save();

		return Redirect::to('/persons/');
	}

	public function delete($id)
	{
		if($id != null)
		{
			$oldPerson = Person::where('personId', $id)->first();
			$oldPerson->isActive = false;
			$oldPerson->fill($oldPerson->toArray());
			$oldPerson->save();
		}

		return Redirect::to('/persons/');
	}

	public function store(PersonRequest $request)
	{
		$input = $request->all();
		/*$validator = Validator::make($request->all(), [
			
		]);
		if ($validator->fails()) {
			return redirect('persons/create')
				->withErrors($validator)
				->withInput();
		}*/
		$person = [];
		$person['personId'] = md5(microtime() . rand() . rand());
		$person['fname'] = $input['fname'];
		$person['lname'] = $input['lname'];
		$person['emails'] = $input['emails'];
		$person['phones'] = $input['phones'];
		$person['isActive'] = true;

		$person = Person::create($person);
		return Redirect::to('/persons/');
	}
}
