<?php

namespace App;

use Jenssegers\Mongodb\Model;

class Client extends Model
{
    //
	protected $collection = "Clients";

	protected $primaryKey = 'clientId';

	protected $attributes = [

		'name'       => '',
		'personId'     => '',
		'phones'     => '',
		'emails'         => '',
		"isActive"=>false,

	];

	protected $fillable = [
		'clientId',
		'name'  ,
		'personId',
		'phones',
		'emails'    ,
		"isActive", 
		
		
	];
}
