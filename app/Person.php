<?php

namespace App;

use Jenssegers\Mongodb\Model;

class Person extends Model
{
	protected $collection = "Persons";

	protected $primaryKey = 'personId';

	protected $attributes = [

		'fname'       => '',
		'lname'     => '',
		'phones'     => '',
		'emails'         => '',
		"isActive"=>false,
		'prefix' => '',
		
	];

	protected $fillable = [

		'personId',
		'fname',
		'lname',
		'phones',
		'emails',
		'isActive',
		'prefix',
	];
}
