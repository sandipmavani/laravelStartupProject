<?php

namespace App\Console\Commands;

use App\Libraries\Repositories\PersonRepository;
use Illuminate\Console\Command;

class addField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addField:field';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add New Field';
    public $personRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PersonRepository $personRepository)
    {
        parent::__construct();
        $this->personRepository = $personRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $field = 'prefix';
        $this->personRepository->addNewFieldInPerson($field);
    }
}
